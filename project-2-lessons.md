# Project Lessons

- Pyplot is really easy to use and very forgiving

- DPI for graphs should be >200 so that the resolution is good enough to see even when pdfs aren't zoomed in
    - I'm using DPI = 96*3 = 288 and am happy with that
    - multiple of 96 is just because dpi is usually a multiple of 96, it can be anything

- JDL2 allows storage of data in a format similar to HDL2 but that also includes julia datatype info 

