% Project 2 Main Document

\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\usepackage{fontspec}
\setmainfont{Liberation Serif}

\usepackage{polyglossia}
\setmainlanguage{english}

\usepackage{geometry}
\geometry{letterpaper, total={6.5in,9in}}
 
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{ {./images/} }

\DeclareMathOperator{\erf}{erf}
\DeclareMathOperator{\E}{\mathbb{E}}


\begin{document}

\begin{flushleft}
\begin{Huge}
CMPE320 Probability, Statistics, and Random Processes \\
Kira Singla (They/Them) \\
ksingla1@umbc.edu \\
\end{Huge}
\end{flushleft}

\section*{Project 2}

\subsection*{Introduction}

This project seeks to analyze the properties of transformations of random variables in an experimental approach, modeling a transmitted signal as it is received by different models of receiver.
For all the following equations, the following parameter values should be assumed: \( A = 1.5, \sigma^2 = 0.225, k = 4 \).



\newpage
\subsection*{Simulation and Discussion}

\subsubsection*{Problem 2.1}

\begin{figure}[t]
    \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{problem-2-1-fig-1-scatter.png}
    \caption{A scatter plot of 100,000 samples from $R$}
    \label{fig:prob1a}
    \end{subfigure}
    \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{problem-2-1-fig-2-hist.png}
    \caption{The histogram of all samples versus the PDF}
    \label{fig:prob1b}
    \end{subfigure}
\caption{Plots for Problem 2.1}
\label{fig:prob1}
\end{figure}


% Question 1
% Using (2), write out the analytical form of \( f_R(r) \) as a function of \(r\) and \(A\). 
% Provide a scatterplot of the simulated values of \(R\) as a function of the index in your array

\qquad The probabilities of \(-A\) and \(+A\) are equal, such that \( f_A(+A) = f_A(-A) = 0.5\), thus the probability density function of \(R\) is as follows:

\begin{equation}
f_R(r) = \frac{1}{2\sqrt{2{\pi}{\sigma^2}}}
        ( \exp( \frac{-(r - A)^2}{2{\sigma^2}} ) +\exp( \frac{-(r + A)^2}{2{\sigma^2}} ) )
\end{equation}


% Question 2
% Explain how the plot represents the signal model. Do the samples cluster around the values of \(+A\) and \(-A\)

The scatter plot in Figure \ref{fig:prob1a} shows that the received signals are clustered around \(+A\) and \(-A\), but with a random amount of variability turning the data into two clouds around \(+A\) and \(-A\), instead of being exactly either \(+A\) or \(-A\) like the transmitted signal. 
This matches the expectation that the received signal will be the transmitted signal plus a random normally distributed amount of noise.


% Question 3
% On a new figure, plot a scaled histogram of the data alongside the analytical result of \( f_R(r) \). 
% Discuss why the histogram and the pdf plot looks like it does.

The histogram and PDF in Figure \ref{fig:prob1b} closely match, both appearing to represent two side by side bell curves. 
This matches expectations as the random noise being added to the original signal is Gaussian, and the original signal has two potential values, so the data is roughly two different Gaussians centered at \(+A\) and \(-A\). 
This histogram, and all future histograms in this project, use a bin width of \(\frac{1}{4}\). 

The expected value of the computed data is as follows:
\begin{gather*}
\mu_{R} = -0.0035588635001267558
\end{gather*}



\newpage
\subsubsection*{Problem 2.2}

\begin{figure}[t]
    \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{problem-2-2-fig-1-scatter.png}
    \caption{A scatter plot of $S1$ versus the original $R$ values}
    \label{fig:prob2a}
    \end{subfigure}
    \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{problem-2-2-fig-2-hist.png}
    \caption{The histogram of all samples versus the PDF}
    \label{fig:prob2b}
    \end{subfigure}
\caption{Plots for Problem 2.2}
\label{fig:prob2}
\end{figure}

% Question 4
% Using the CDF method developed in class, analytically derive the probability density function.

\qquad As shown in Appendix C, the PDF of \(S\) is 

\begin{align*}
f_S(s) = 
    \begin{cases}
    0 ,\, & s<0 \\
    0.5 ,\, & s=0 \\
    \frac{1}{k} f_R(\frac{s}{k}) ,\, & s>0
    \end{cases}
\end{align*}

% Question 5
% Does the scatterplot match your expectations? Why or why not. 

The scatter plot in Figure \ref{fig:prob2a} closely matches the graph of \( g(R) \), and this is what was expected given the graph contains a subset of the points of \( g(R) \).
This also explains why the graph has many gaps and seems to "fade out" toward the edges of the domain.

The histogram of the experimental data in Figure \ref{fig:prob2b} shows some unusual behavior around \( s = 0 \), spiking to 4x the height of the PDF.
If the bin width is decreased, this is observed to increase even more dramatically, such that \( \text{binwidth}^{-1} * \text{height} = 0.5 \), so this is just a relic of the fact that a histogram cannot properly show a unit impulse in the underlying data.

The expected values of the computed data are as follows:
\begin{gather*}
\mu_{S1} = 2.9922838837347125 \\
g(\mu_{R}) = 0
\end{gather*}



\newpage
\subsubsection*{Problem 2.3}

\begin{figure}[t]
    \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{problem-2-3-fig-1-scatter.png}
    \caption{A scatter plot of $S2$ versus the original $R$ values}
    \label{fig:prob3a}
    \end{subfigure}
    \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{problem-2-3-fig-2-hist.png}
    \caption{The histogram of all samples versus the PDF}
    \label{fig:prob3b}
    \end{subfigure}
\caption{Plots for Problem 2.3}
\label{fig:prob3}
\end{figure}

% Question 6
% Using the analytical method developed in class, derive the probability density function.

\qquad As shown in Appendix D, the PDF of \(S\) has a domain of \( s \in [0, \infty) \) and is \[f_S(s) = \frac{2}{k} f_R(\frac{s}{k})\]

% Question 7
% Does the scatterplot match your expectations? Why or why not. 

As in the previous problem, the scatter plot in Figure \ref{fig:prob3a} closely mimics the graph of \( g(R) \), which is to be expected because every point on the plot is an ordered pair \( (r, g(r) )\), however because the values of \(r\) are random samples from \(R\) and do not represent the entire span of the random variable, there are several "holes" in this graph.

The expected values of the computed data are as follows:

\begin{gather*}
\mu_{S2} = 5.998803221469934 \\
g(\mu_{R}) = 0.014235454000507023
\end{gather*}



\newpage
\subsubsection*{Problem 2.4}

\begin{figure}[t]
    \begin{subfigure}{0.45\linewidth}
    \includegraphics[width=\linewidth]{problem-2-4-fig-1-scatter.png}
    \caption{A scatter plot of $S3$ versus the original $R$ values}
    \label{fig:prob4a}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
    \includegraphics[width=\linewidth]{problem-2-4-fig-2-hist.png}
    \caption{The histogram of all samples versus the PDF}
    \label{fig:prob4b}
    \end{subfigure}
\caption{Plots for Problem 2.4}
\label{fig:prob4}
\end{figure}


% Question 8
% Using the analytical method developed in class, analytically derive the probability density function.

\qquad As shown in Appendix E, the PDF of \(S\) has a domain of \( s \in [0, \infty) \) and is 

\begin{align*}
f_S(s) = 
\begin{cases}
f_R(0),\, &s = 0\\
\frac{1}{\sqrt{sk}} f_R(\sqrt{\frac{s}{k}}) ,\, &s>0
\end{cases}
\end{align*}


% Question 9
% Does the scatterplot match your expectations? Why or why not.

For the same reason as in the previous two problems, the scatter plot in Figure \ref{fig:prob4a} matches expectations in every way.

This histogram and PDF in Figure \ref{fig:prob4b} have some differences that are difficult to explain.
The PDF has a spike around \( s = 0 \), likely due to a singularity being formed at \( s = 0 \) if the expression for \( s > 0 \) was on a wider domain, however the histogram appears smooth in this region.
Intuitively, given this transformation just scales the original data, we assume the shape of this data should match the shape of the original data for \(R\), and the histogram does show this, however the PDF does not.
This means that there is potential for the PDF to be incorrect, or there to be some caveat to the graph of the PDF that has not been fully explored. 

The expected values of the computed data are as follows:
\begin{gather*}
\mu_{S3} = 9.892672096279096 \\
g(\mu_{R}) = 5.066203765013785e-5
\end{gather*}


\newpage
\subsubsection*{Problem 2.5}

% Question 10
% Is there a consistent inequality relationship that extends across the three cases? That is, is
% one of these values consistently larger or smaller than the other across the differences in the
% detection method? Can you guess the general rule, which is known as Jensen’s Inequality?

The expected values of the raw data are as following:
\begin{gather*}
\mu_{R} = -0.0035588635001267558 \\
\mu_{S1} = 2.9922838837347125 \\
g(\mu_{R}) = 0 \\
\mu_{S2} = 5.998803221469934 \\
g(\mu_{R}) = 0.014235454000507023 \\
\mu_{S3} = 9.892672096279096 \\
g(\mu_{R}) = 5.066203765013785e-5
\end{gather*}

There appears to be a clear pattern of \( \mu_{Sn} >> g_n(\mu_{R}) \) in this data.
There also seems to be some sort of linear stepping in the expected values themselves, because they are sequenced as \( \sim 0, \sim 3, \sim 6, \sim 9 \). 
This is probably related to the fact that the magnitude (meaning the square integral on a finite region \( (-\alpha, \alpha) \)) of each transform is larger than the preceding one. 
Jensen's Inequality states that in general, \(\phi( \E[X]) \le \E[ \phi(X) ]\), and matches this observation in the data.


\subsection*{What I Learned}

\qquad This project, much more than the previous one, has been a journey of refinement and improvement. 
Whereas I spent the previous project experimenting with new tools (Fortran and PLplot), I decided to use tools more familiar to me this time.
Julia as a language isn't something I have much experience with, however it works a lot like python, and the graphing tool I used is actually written in python (I also have experience using this tool in python). 
I have previously worked with batch data (as in project 1, but also other places), however many of the strategies I have used have been unrefined and poorly organized, making it hard to scale them. 
For this project, I took advantage of the fact that Julia doesn't require function interfaces or modules in order to build a low effort but highly modular codebase for my project, which cut my code re-use by a large portion. 
Not only was this more efficient, it eliminated sources of error where I'd have to keep several identical blocks of code updated with improvements.
Organizing functions across different files had the added benefit of shortening file sizes and making it easier to find the code block I want to edit.
Another skill I practiced in this project was writing my entire data to disk in a structured way (using HDF5) that allowed for future retrieval of the data.

I found that this project's difficulty was underwhelming, and the computational elements were only ever difficult in cases where I had to re-implement code in Julia that I had previously written in Fortran (which works \textit{very} differently, so it was not simple), however what I have seen from a lot of my peers in the class is that they struggled with these things, so I do not think it would be fair to increase the difficulty or complexity of this project. I estimate that this project took me 15-20 hours to complete in total, and provided skills and workflow management strategies that will make future projects even more efficient to complete. 


\newpage
\subsection*{Appendix A: Deriving the CDF of $R$} \label{appendixA}

\begin{equation}
f_R(r) = \frac{1}{2\sqrt{2{\pi}{\sigma^2}}} 
         ( \exp( \frac{-(r - A)^2}{2{\sigma^2}} ) +\exp( \frac{-(r + A)^2}{2{\sigma^2}} ) )
\end{equation}

\begin{equation}
F_R(r) = \int_{-\infty}^{r} f_R(s) ds
\end{equation}

\begin{align*}
F_R(r) =& \int_{-\infty}^{r} \frac{1}{2\sqrt{2{\pi}{\sigma^2}}}( \exp( \frac{-(s - A)^2}{2{\sigma^2}} ) +\exp( \frac{-(s + A)^2}{2{\sigma^2}} ) ) ds \\
=& \int_{-\infty}^{r} \frac{1}{2\sqrt{2{\pi}{\sigma^2}}} \exp( \frac{-(s - A)^2}{2{\sigma^2}} ) ds +\int_{-\infty}^{r} \frac{1}{2\sqrt{2{\pi}{\sigma^2}}} \exp( \frac{-(s + A)^2}{2{\sigma^2}} ) ds
\end{align*}

\begin{gather*}
\text{Let } u = \frac{s-A}{\sqrt{2\sigma^2}} \\
du = \frac{1}{\sqrt{2\sigma^2}}ds \\
s_1 = -\infty \Rightarrow u_1 = -\infty,\, 
s_2 = r \Rightarrow u_2 = \frac{r-A}{\sqrt{2\sigma^2}} \\
\int_{-\infty}^{r} \frac{1}{2\sqrt{2{\pi}{\sigma^2}}} \exp( \frac{-(s - A)^2}{2{\sigma^2}} ) ds = \int_{-\infty}^{\frac{r-A}{\sqrt{2\sigma^2}}} \frac{1}{2\sqrt{\pi}} \exp(-u^2) du \\
=  \frac{1}{4} ( \int_{0}^{\frac{r-A}{\sqrt{2\sigma^2}}} \frac{2}{\sqrt{\pi}}\exp(-u^2) du
               - \int_{0}^{-\infty}                      \frac{2}{\sqrt{\pi}}\exp(-u^2) du) \\
= \frac{1}{4} ( \erf(\frac{r-A}{\sqrt{2\sigma^2}}) - \erf(-\infty) ) \\
= \frac{1}{4} ( \erf(\frac{r-A}{\sqrt{2\sigma^2}}) + 1 )
\end{gather*}

\begin{gather*}
\text{Let } u = \frac{s+A}{\sqrt{2\sigma^2}} \\
du = \frac{1}{\sqrt{2\sigma^2}}ds \\
s_1 = -\infty \Rightarrow u_1 = -\infty ,\,
s_2 = r       \Rightarrow u_2 = \frac{r+A}{\sqrt{2\sigma^2}} \\
\int_{-\infty}^{r} \frac{1}{2\sqrt{2{\pi}{\sigma^2}}} \exp( \frac{-(s+A)^2}{2{\sigma^2}} ) ds = \int_{-\infty}^{\frac{r+A}{\sqrt{2\sigma^2}}} \frac{1}{2\sqrt{\pi}} \exp(-u^2) du \\
=  \frac{1}{4} ( \int_{0}^{\frac{r+A}{\sqrt{2\sigma^2}}} \frac{2}{\sqrt{\pi}}\exp(-u^2) du
               - \int_{0}^{-\infty}                      \frac{2}{\sqrt{\pi}}\exp(-u^2) du) \\
= \frac{1}{4} ( \erf(\frac{r+A}{\sqrt{2\sigma^2}}) - \erf(-\infty) ) \\
= \frac{1}{4} ( \erf(\frac{r+A}{\sqrt{2\sigma^2}}) + 1 )
\end{gather*}

\begin{equation}
F_R(r) = 0.25( \erf(\frac{r-A}{\sqrt{2\sigma^2}}) + \erf(\frac{r+A}{\sqrt{2\sigma^2}}) + 2)
\end{equation}


\subsection*{Appendix B: \texttt{erf()} in Julia} \label{appendixB}

Julia does not implement the error function, so the package \href{https://specialfunctions.juliamath.org/stable/}{SpecialFunctions.jl} is used. This provides \texttt{erf()} where 
\begin{equation}
\erf(x) = \frac{2}{\sqrt{\pi}} \int_{0}^{x} \exp(-s^2) ds
\end{equation}

% Function in docs https://specialfunctions.juliamath.org/stable/functions_list/#SpecialFunctions.erf


\subsection*{Appendix C: Deriving the CDF for Problem 2.2}

\qquad Recall that \(S = \begin{cases} kR, R \ge 0\\ 0, R < 0 \end{cases} \). Using the CDF method, we describe how each region of \(S\) transforms separately. 

For \(S \ge 0 \):
\begin{align*}
\Pr[S \le s] 
=& \Pr[S \le 0] + \Pr[0 < S \le s] \\
=& \Pr[R \le 0] + \Pr[0 < R \le \frac{s}{k}] \\
=& F_R(0) + ( F_R(\frac{s}{k}) - F_R(0) ) \\
=& F_R(\frac{s}{k})
\end{align*}

For \( S < 0 \):
\[ \Pr[S \le s] = \Pr[S \le s < 0] = 0 \]

Thus, the CDF of \(S\) is

\begin{align*}
F_S(s) = 
    \begin{cases}
    0,\, & s < 0 \\
    F_R(\frac{s}{k}),\, & s \ge 0
    \end{cases}
\end{align*}

We take the derivative of this to obtain \(f_S\), and note that \(F_R(0) = 0.5\), which provides a more explicit expression

\begin{align*}
f_S(s) = 
    \begin{cases}
    0 ,\, & s < 0\\
    0.5 ,\, & s = 0\\
    \frac{1}{k} f_R(\frac{s}{k}) ,\, & s > 0
    \end{cases}
\end{align*}


\subsection*{Appendix D: Deriving the CDF for Problem 2.3}

\qquad Recall that \(S = k \vert R \vert\), so we have the following relationship:

\begin{align*}
S = 
\begin{cases} 
-kR,\, & R < 0 \\
kR,\,  & R \ge 0
\end{cases}
\end{align*}

This function has two regions where it is invertible, \( r \in (-\infty, 0) \) and \( r \in [0, \infty) \). This means that the PDF of \(S\) will be the sum of two terms, one for each region, so:

\[
f_S(s) = \sum_{i=1}^{2} \frac{1}{ \vert \frac{ds}{dr} \vert} f_R(r) ]_{r = g_i^{-1}(s)}
\]

We can compute the terms for \(i=1\) and \(i=2\) separately and add them. First we tackle \(i=1\), which corresponds to \( r \in (-\infty, 0) \).

\begin{gather*}
g_1(r) = s = -r \\
g_1^{-1}(s) = r = -\frac{s}{k} \\
\frac{ds}{dr} = -k
\end{gather*}

Thus, the term for \(i=1\) is \[ \frac{1}{k} f_R(r) ]_{r = -\frac{s}{k}} \] and applies for \( s > 0 \). We apply the evenness of \(f_R\) to simplify this into \[ \frac{1}{k} f_R(\frac{s}{k}) \]

We next tackle \(i=2\), corresponding to \( r \in [0, \infty) \).

\begin{gather*}
g_1(r) = s = r \\
g_1^{-1}(s) = r = \frac{s}{k} \\
\frac{ds}{dr} = k
\end{gather*}

Thus, the term for \(i=2\) is \[ \frac{1}{k} f_R(r) ]_{r = \frac{s}{k}} \] and applies for \( s \ge 0 \). This simplifies to \[ \frac{1}{k} f_R(\frac{s}{k}) \] 

The sum of the terms for \(i=1\) and \(i=2\), and thus the PDF of \(S\), has a domain of \( s \ge 0 \) and is

\begin{equation}
f_S(s) = \frac{2}{k} f_R(\frac{s}{k}) 
\end{equation}



\subsection*{Appendix E: Deriving the CDF for Problem 2.4} \label{appendixE}

Recall that \(S = kR^2\). This function is invertible on three separate regions, \( r \in (-\infty, 0) \), \( r \in (0, \infty) \), and \( r = 0 \), so the PDF is the sum of three terms as in the previous problem.

We first compute the term for the first region, \( r \in (-\infty, 0) \). 

\begin{gather*}
g_1(r) = s = kr^2 \\
g_1^{-1}(s) = r = -\sqrt{\frac{s}{k}} \\
\frac{ds}{dr} = 2kr \\
\vert \frac{ds}{dr} \vert = -2kr
\end{gather*}

Thus, the first term has a domain of \(s>0\) and is \[\frac{1}{-2kr} f_R(r) ]_{r=-\sqrt{\frac{s}{k}}} = \frac{1}{2\sqrt{sk}} f_R( \sqrt{\frac{s}{k}}) \]

We next compute the term for the second region, \( r \in (0, \infty) \).

\begin{gather*}
g_1(r) = s = kr^2 \\
g_1^{-1}(s) = r = \sqrt{\frac{s}{k}} \\
\frac{ds}{dr} = 2kr \\
\vert \frac{ds}{dr} \vert = 2kr
\end{gather*}

Thus, the second term has a domain of \(s>0\) and is \[\frac{1}{2kr} f_R(r) ]_{r=\sqrt{\frac{s}{k}}} = \frac{1}{2\sqrt{sk}} f_R( \sqrt{\frac{s}{k}}) \]

The third term is found more intuitively, observing that \( f_R(0) = f_S(0) \), because the transformation between them is only a (nonlinear) scaling, and not a translation. 

Thus, the PDF has a domain of \(s \ge 0\) and is as follows

\begin{align}
f_S(s) = 
\begin{cases}
f_R(0),\, &s = 0\\
\frac{1}{\sqrt{sk}} f_R(\sqrt{\frac{s}{k}}) ,\, &s>0
\end{cases}
\end{align}

\end{document}
