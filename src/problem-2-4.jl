#! /usr/bin/env julia 
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Problem 2.4
=#

using PyPlot
using JLD2
include("pdf-funcs.jl")
include("data-binning.jl")
include("random-var-funcs.jl")


A::Float64 = 1.5
varN::Float64 = 0.225
n::Integer = 100000
k::Float64 = 4.0
R = load_object("../data/Received_Data.jld2")
binsize::Rational{Int64} = 1//4


## Transform data

S3 = genS3(R, k)


## Plot scatterplot

figure(7)
xlabel(raw"$R$ ($r$)")
ylabel(raw"$S3$ ($s$)")
title(raw"Scatterplot of $S3$ versus $R$")
scatter(R, S3, s=1, label=raw"$S3(r)$")
figlegend()
savefig("../images/problem-2-4-fig-1-scatter.png", dpi=288)


## Bin data

maxval = findmax(S3)
minval = findmin(S3)
numbins = (maxval - minval) * denominator(binsize)
binvals = create_binvals(binsize, minval, numbins)
binned_S3 = bin_data(S3, binsize, binvals, minval)


## Plot histogram and PDF

figure(8)
xlabel(raw"$s$")
ylabel("Probability (Density)")
title(raw"Experimental data versus $f_{S3}(s)$")
hist(binvals, bins=binvals, weights=binned_S3, align="left", label="data")
x = range(minval, step=0.01, stop=maxval)
plot(x, fS3.(x, A, varN, k), label=raw"$f_{S3}(s)$")
figlegend()
savefig("../images/problem-2-4-fig-2-hist.png", dpi=288)

## Compute data mean

ER = find_mean(R)
ES3 = find_mean(S3)

EgR = k* ER^2

println(raw"\mu_{S3} = ", ES3)

println(raw"g(\mu_{R}) = ", EgR)
