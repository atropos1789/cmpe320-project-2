#! /usr/bin/env julia 
#= 
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Functions for the analytic pdfs of the random variables
=#

using SpecialFunctions


function fR(r::Float64, mu::Float64, varN::Float64)::Float64
    # PDF of R
    part1 = -1 * ((r - mu)^2 / (2.0*varN) )
    part2 = -1 * ((r + mu)^2 / (2.0*varN) )
    part3 = 0.5/sqrt(2.0*pi*varN)
    return part3*(exp(part1) + exp(part2))
end


function fS1(s::Float64, A::Float64, variance::Float64, k::Float64)::Float64
    # PDF of S1, defined for all s
    if s < 0
        return 0.0
    elseif s == 0
        return 0.5
    else 
        return (1/k)*fR(s/k, A, variance)
    end
end


function fS2(s::Float64, A::Float64, variance::Float64, k::Float64)::Float64
    # PDF of S2, only defined for s >= 0
    return 2.0/k * fR(s/k, A, variance)
end


function fS3(s::Float64, A::Float64, variance::Float64, k::Float64)::Float64
    # pdf of S3, only defined for s >= 0
    if s == 0
        return fR(0.0, A, variance)
    else
        return 1/((k*s)^0.5) * fR((s/k)^0.5, A, variance)
    end
end


# CDFs (not used in code)

function FR(r::Float64, mu::Float64, varN::Float64)::Float64
    # CDF of R
    part1 = (r - A)/(2*varN)
    part2 = (r + A)/(2*varN)
    return 0.25*(erf(part1) + erf(part2) + 2)
end


function FS1(s::Float64, A::Float64, variance::Float64, k::Float64)::Float64
    # CDF of S1
    if s < 0
        return 0.0
    elseif s == 0
        return 0.5
    else
        part1 = (s - A*k)/(k*(2*variance)^0.5)
        part2 = (s + A*k)/(k*(2*variance)^0.5)
        return 0.25*(erf(part1) + erf(part2) + 2)
    end
end
