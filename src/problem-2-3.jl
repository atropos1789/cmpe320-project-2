#! /usr/bin/env julia 
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Problem 2.3
=#

using PyPlot
using JLD2
include("pdf-funcs.jl")
include("data-binning.jl")
include("random-var-funcs.jl")


A::Float64 = 1.5
varN::Float64 = 0.225
n::Integer = 100000
k::Float64 = 4.0
R = load_object("../data/Received_Data.jld2")
binsize::Rational{Int64} = 1//4


## Transform data

S2 = genS2(R, k)


## Plot scatterplot

figure(5)
xlabel(raw"$R$ ($r$)")
ylabel(raw"$S2$ ($s$)")
title(raw"Scatterplot of $S2$ versus $R$")
scatter(R, S2, s=1, label=raw"$S2(r)$")
figlegend()
savefig("../images/problem-2-3-fig-1-scatter.png", dpi=288)


## Bin data

maxval = findmax(S2)
minval = findmin(S2)
numbins = (maxval - minval) * denominator(binsize)
binvals = create_binvals(binsize, minval, numbins)
binned_S2 = bin_data(S2, binsize, binvals, minval)


## Plot histogram and PDF

figure(6)
xlabel(raw"$s$")
ylabel("Probability Density")
title(raw"Experimental data versus $f_{S2}(s)$")
hist(binvals, bins=binvals, weights=binned_S2, align="left", label="data")
x = range(minval, step=0.01, stop=maxval)
plot(x, fS2.(x, A, varN, k), label=raw"$f_{S2}(s)$")
figlegend()
savefig("../images/problem-2-3-fig-2-hist.png", dpi=288)

## Compute data mean

ER = find_mean(R)
ES2 = find_mean(S2)

EgR = k*abs(ER)

println(raw"\mu_{S2} = ", ES2)

println(raw"g(\mu_{R}) = ", EgR)
