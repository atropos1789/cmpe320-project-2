#! /usr/bin/env julia 
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Problem 2.2
=#

using PyPlot
using JLD2
include("pdf-funcs.jl")
include("data-binning.jl")
include("random-var-funcs.jl")


A::Float64 = 1.5
varN::Float64 = 0.225
n::Integer = 100000
k::Float64 = 4.0
R = load_object("../data/Received_Data.jld2")
binsize::Rational{Int64} = 1//4


## Transform data

S1 = genS1(R, k)


## Plot scatterplot

figure(3)
xlabel(raw"$R$ ($r$)")
ylabel(raw"$S1$ ($s$)")
title(raw"Scatterplot of $S1$ versus $R$")
scatter(R, S1, s=1, label=raw"$S1(r)$")
figlegend()
savefig("../images/problem-2-2-fig-1-scatter.png", dpi=288)


## Bin data

maxval = findmax(S1)
minval = findmin(S1)
numbins = (maxval - minval) * denominator(binsize)
binvals = create_binvals(binsize, minval, numbins)
binned_S1 = bin_data(S1, binsize, binvals, minval)


## Plot histogram and PDF

figure(4)
xlabel(raw"$s$")
ylabel("Probability (Density)")
title(raw"Experimental data versus $f_{S1}(s)$")
hist(binvals, bins=binvals, weights=binned_S1, align="left", label="data")
x = range(minval, step=0.01, stop=maxval)
plot(x, fS1.(x, A, varN, k), label=raw"$f_{S1}(s)$")
figlegend()
savefig("../images/problem-2-2-fig-2-hist.png", dpi=288)

## Compute data mean

ER = find_mean(R)
ES1= find_mean(S1)

if ER >= 0
    EgR = k* ER^2
else 
    EgR = 0
end

println(raw"\mu_{S1} = ", ES1)

println(raw"g(\mu_{R}) = ", EgR)
