#! /usr/bin/env julia 
#= 
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Problem 2.1
=#

using PyPlot
using JLD2
include("pdf-funcs.jl")
include("data-binning.jl")
include("random-var-funcs.jl")


A::Float64 = 1.5
varN::Float64 = 0.225
n::Integer = 100000
k::Float64 = 4.0
binsize::Rational{Int64} = 1//4


## Create and save random data

R = genR(n, A, varN)
save_object("../data/Received_Data.jld2", R)


## Plot scatterplot

figure(1)
xlabel(raw"Trial Number ($n$)")
ylabel(raw"Result ($r$)")
title(raw"Scatterplot of $R(n) = r$")
scatter(range(0, length=n),R, s=1, label=raw"$R(n)$")
figlegend()
savefig("../images/problem-2-1-fig-1-scatter.png", dpi=288)


## Bin data

maxval = findmax(R)
minval = findmin(R)
numbins = (maxval - minval) * denominator(binsize)
binvals = create_binvals(binsize, minval, numbins)
binned_R = bin_data(R, binsize, binvals, minval)


## Plot histogram and PDF

figure(2)
xlabel(raw"$r$")
ylabel("Probability (Density)")
title(raw"Experimental data versus $f_R(r)$")
# histogram needs to be aligned left because of unknown reasons,
# but this is necessary to have properly centered bins
hist(binvals, bins=binvals, weights=binned_R, align="left", label="data")
x = range(minval, step=0.01, stop=maxval)
plot(x, fR.(x, A, varN), label=raw"$f_R(r)$")
figlegend()
savefig("../images/problem-2-1-fig-2-hist.png", dpi=288)


## Compute data mean

ER = find_mean(R)

println(raw"\mu_{R} = ", ER)
println(mean)
