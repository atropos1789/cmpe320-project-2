#! /usr/bin/env julia 
#= 
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Functions for generating and transforming random variables
=#


function genX(n::Integer, A::Float64)::Vector{Float64}
    # generate n values from the random variable X
    # use rand() to choose n values randomly from S = (-A, A)
    rand((-A, A), n)
end


function genN(n::Integer, varN::Float64)::Vector{Float64}
    # generate n values from the random variable N
    # use randn() to choose n values randomly from N(0,1) and map them to N(0,sqrt(varN))
    randn(Float64, n) * sqrt(varN)
end


function genR(n::Integer, A::Float64, varN::Float64)::Vector{Float64}
    # generate n values from the random variable R
    X = genX(n, A)
    N = genN(n, varN)
    R = X + N
end


function genS1(R::Vector{Float64}, k::Float64)::Vector{Float64}
    # generate n values from the random variable S1
    S1 = Vector{Float64}(undef, length(R))
    for i = 1:length(R)
        if R[i] < 0
            S1[i] = 0
        else
            S1[i] = k*R[i]
        end
    end
    return S1
end


function genS2(R::Vector{Float64}, k::Float64)::Vector{Float64}
    # generate n values from the random variable S2
    S2 = Vector{Float64}(undef, length(R))
    for i = 1:length(R)
        S2[i] = k*abs(R[i])
    end
    return S2
end


function genS3(R::Vector{Float64}, k::Float64)::Vector{Float64}
    # generate n values from the random variable S3
    S3 = Vector{Float64}(undef, length(R))
    for i = 1:length(R)
        S3[i] = k*(R[i])^2
    end
    return S3
end


function find_mean(R::Vector{Float64})::Float64
    # find the mean from a dataset 
    mean = 0
    for i = 1:length(R)
        mean += R[i]
    end
    mean = mean / length(R)
    return mean
end
