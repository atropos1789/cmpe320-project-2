#! /usr/bin/env julia 
#= 
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Code entry point
=#

# run the code to solve each problem
include("problem-2-1.jl")
include("problem-2-2.jl")
include("problem-2-3.jl")
include("problem-2-4.jl")
