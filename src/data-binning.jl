#! /usr/bin/env julia 
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 2
Functions to bin data
=#


function findmax(data::Vector{Float64})
    # find the maximum value within the dataset
    maxval = 0.0
    for i in 1:length(data)
        (data[i] >= maxval) && (maxval = data[i])
    end
    maxval = ceil(maxval)
    return maxval
end


function findmin(data::Vector{Float64})
    # find the minimum value within the dataset
    minval = 0.0
    for i in 1:length(data)
        (data[i] <= minval) && (minval = data[i])
    end
    minval = floor(minval)
    return minval
end


function create_binvals(binsize, minval, numbins)::Vector{Float64}
    # determine the center of each bin for the data
    return Float64[ k * binsize + minval for k = 0:numbins ]
end


function bin_data(data::Vector{Float64}, bin_size::Rational{Int64}, bin_vals::Vector{Float64}, minval::Float64)::Vector{Float64}
    # transform unbinned data into binned data
    binned_data = zero(bin_vals)
    for i in 1:length(data)
        bin_center = round(data[i]*denominator(binsize))/denominator(binsize)
        bin_num = (bin_center - minval)*denominator(binsize) + 1
        bin_num = convert(Int64, bin_num)
        binned_data[bin_num] += 1
    end
    normed_binned_data = binned_data * denominator(binsize) / length(data)
    return normed_binned_data    
end
